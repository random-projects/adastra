/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra;

import AdAstra.Database.DatabaseController;
import AdAstra.Module.AbstractModule;
import AdAstra.Module.ModuleController;
import AdAstra.Module.Planet.PlanetModule;
import AdAstra.SwingUI.InterfaceController;

import java.awt.*;
import java.util.Observable;
import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 02:36
 * To change this template use File | Settings | File Templates.
 */
public class AdAstraCore extends Observable {
    protected InterfaceController userInterface;
    protected DatabaseController database;
    protected ModuleController module;

    protected GalaxyModel model;

    public static void main(String[] args) {
        new AdAstraCore();
    }

    public AdAstraCore(){
        this.module = new ModuleController();
        this.database = new DatabaseController();
        this.model = new GalaxyModel(this);
        this.userInterface = new InterfaceController(this.model);

        module.add(new PlanetModule());

        buildGalaxy(this.model);
    }

    public void buildGalaxy(GalaxyModel gm){
        Random r = new Random();
        for(int i=0; i<50+r.nextInt(150); i++){
            gm.addSector(buildSector(r.nextInt(450) + 5, r.nextInt(450) + 5));
        }
    }

    public Sector buildSector(int x, int y){
        Sector s = new Sector("", new Point(x,y));

        for(AbstractModule m : module.getModules()){
            m.buildSector(s);
        }

        return s;
    }

}
