/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 02:43
 * To change this template use File | Settings | File Templates.
 */
public class MapAsset{
    protected String name;
    protected Player owner;
    protected Point location;
    protected Polygon bounds;
    protected Collection<Component> components;

    //position related
    protected int sides = 6;
    protected int radius = 50;

    public MapAsset(String name, Player owner, Point location, int sides, int radius) {
        this.name = name;
        this.owner = owner;
        this.bounds = new Polygon();
        this.components = new ArrayList<Component>();
        this.sides = sides;
        this.radius = radius;
        setLocation(location);
    }

    public String getName(){
        return name;
    }

    public String getType(){
        return "mapasset";
    }

    public Color getColor(){
        return Color.white;
    }

    public void setOwner(Player p){}
    public Player getOwner(){
        return owner;
    }

    public void setLocation(Point p){
        location = p;
        bounds.reset();

        //update the bounds of the shape
        double angle = 360.0/sides;
        for(int i=0; i<sides; i++){
            int y = (int)(Math.sin(Math.toRadians(angle*i)) * radius);
            int x = (int)(Math.cos(Math.toRadians(angle*i)) * radius);
            bounds.addPoint(p.x+x, p.y+y);
        }
    }

    public Point getLocation(){
        return location;
    }

    public boolean inBounds(Point p){
        return bounds.contains(p);
    }

    public Polygon getPolygon(){
        return bounds;
    }

    public void addComponent(Component c){}

    public boolean hasComponent(Component c){
        return false;
    }

    public Collection<Component> getComponents(){
        return null;
    }

    public Action[] getActions(){
        return null;
    }

    public boolean equals(Object o){
        return false;
    }

    public String toString(){
        return "test asset";
    }

}
