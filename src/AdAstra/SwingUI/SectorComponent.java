/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra.SwingUI;

import AdAstra.MapAsset;
import AdAstra.Sector;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 03:17
 * To change this template use File | Settings | File Templates.
 */
public class SectorComponent extends JComponent {
    protected InterfaceController controller;

    public SectorComponent(InterfaceController ic) {
        this.controller = ic;
        this.setPreferredSize(new Dimension(500, 500));
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    MapAsset m = controller.getSector().getAssetAt(e.getPoint());
                    if (m != null) {
                        controller.selectAsset(m);
                    }
                    return;
                }

                if (e.getButton() == MouseEvent.BUTTON3) {
                    System.out.println("right mouse button");
                }
            }
        });
    }

    public String getName() {
        return "Sector View";
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());

        if (controller.getSector() == null) {
            return;
        }

        for (MapAsset m : controller.getSector().getAssets()) {
            g.setColor(m.getColor());
            g.fillPolygon(m.getPolygon());
        }

        if (controller.getAsset() != null) {
            g.setColor(Color.yellow);
            g.drawPolygon(controller.getAsset().getPolygon());

        }
    }
}
