/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra.SwingUI;

import AdAstra.MapAsset;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 23:46
 * To change this template use File | Settings | File Templates.
 */
public class MapAssetComponent extends JComponent {
    protected InterfaceController controller;

    public MapAssetComponent(InterfaceController controller){
        this.controller = controller;
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());

        if(controller.getAsset() == null){
            return;
        }

        MapAsset a = controller.getAsset();

        g.setColor(Color.white);

        g.drawRect(5,5,getWidth()-10, 30);
        g.drawString(a.getName(), 15, 25);
    }

    public String getName(){
        return "Asset View";
    }
}
