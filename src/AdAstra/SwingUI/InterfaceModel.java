/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra.SwingUI;

import AdAstra.GalaxyModel;
import AdAstra.MapAsset;
import AdAstra.Sector;

import java.awt.*;
import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 20:34
 * To change this template use File | Settings | File Templates.
 */
public class InterfaceModel {
    protected Sector sector;
    protected MapAsset asset;
    protected GalaxyModel galaxy;

    public InterfaceModel(GalaxyModel gm){
        this.sector = null;
        this.asset = null;
        this.galaxy = gm;
    }

    public Sector getSectorAt(Point p){
        return galaxy.getSectorAt(p);
    }

    public Sector getSelectedSector(){
        return sector;
    }

    public Collection<Sector> getSectors(){
        return galaxy.getSectors();
    }

    public void setSelectedSector(Sector s){
        this.sector = s;
        this.asset = null;
    }

    public MapAsset getAssetAt(Point p){
        if(sector == null){
            return null;
        }

        return sector.getAssetAt(p);
    }

    public MapAsset getSelectedAsset(){
        return asset;
    }

    public void setSelectedAsset(MapAsset m){
        this.asset = m;
    }

}
