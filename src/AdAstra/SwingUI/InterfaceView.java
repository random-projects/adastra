/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra.SwingUI;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 20:48
 * To change this template use File | Settings | File Templates.
 */
public class InterfaceView extends JFrame {
    protected InterfaceModel model;
    protected InterfaceController controller;

    //Components
    protected GalaxyComponent galaxyComponent;
    protected SectorComponent sectorComponent;
    protected MapAssetComponent assetComponent;

    public InterfaceView(InterfaceController c, InterfaceModel m){
        super("Ad Astra - Development Version");
        this.controller = c;
        this.model = m;
        buildUI();
    }

    protected void buildUI(){
        JTabbedPane tabby = new JTabbedPane();

        galaxyComponent = new GalaxyComponent(model, controller);
        tabby.add(galaxyComponent);

        sectorComponent = new SectorComponent(controller);
        tabby.add(sectorComponent);

        assetComponent = new MapAssetComponent(controller);
        tabby.add(assetComponent);

        this.add(tabby);

        pack();
        setVisible(true);
    }
}
