/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra.SwingUI;

import AdAstra.GalaxyModel;
import AdAstra.Sector;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 04:08
 * To change this template use File | Settings | File Templates.
 */
public class GalaxyComponent extends JComponent{
    protected InterfaceModel model;
    protected InterfaceController controller;

    public GalaxyComponent(InterfaceModel gm, InterfaceController ic){
        this.model = gm;
        this.controller = ic;
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.selectSector(model.getSectorAt(e.getPoint()));
            }
        });
    }


    public String getName(){
        return "Galaxy View";
    }

    public void paintComponent(Graphics g){
        g.setColor(Color.BLACK);
        g.fillRect(0,0,getWidth(),getHeight());

        g.setColor(Color.WHITE);
        for(Sector s : model.getSectors()){
            Point p = s.getLocation();
            g.drawOval(p.x-5, p.y-5, 10,10);
        }

        g.setColor(Color.YELLOW);
        if(controller.getSector() != null){
            Point p = controller.getSector().getLocation();
            g.fillOval(p.x-5, p.y-5, 10, 10);
        }
    }
}
