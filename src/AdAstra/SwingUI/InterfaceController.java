/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra.SwingUI;

import AdAstra.GalaxyModel;
import AdAstra.MapAsset;
import AdAstra.Sector;

/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 02:37
 * To change this template use File | Settings | File Templates.
 */
public class InterfaceController {
    protected InterfaceModel model;
    protected InterfaceView view;

    public InterfaceController(GalaxyModel galaxyModel){
        this.model = new InterfaceModel(galaxyModel);
        this.view  = new InterfaceView(this, this.model);
    }

    public Sector getSector(){
        return model.getSelectedSector();
    }

    public void selectSector(Sector sector){
        model.setSelectedSector(sector);
        view.repaint();
    }

    public MapAsset getAsset(){
        return model.getSelectedAsset();
    }

    public void selectAsset(MapAsset asset){
        model.setSelectedAsset(asset);
        view.repaint();
    }

}