/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra;

import AdAstra.SwingUI.SectorComponent;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;


/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 02:41
 * To change this template use File | Settings | File Templates.
 */
public class GalaxyModel {
    protected List<Sector> sectors;
    protected AdAstraCore core;

    public GalaxyModel(AdAstraCore ac){
        this.core = ac;
        this.sectors = new ArrayList<Sector>();
    }

    public Sector getSectorAt(Point p){
        for(Sector s : sectors){
            if(s.location.x-5 < p.x && s.location.x+5 > p.x){
                if(s.location.y-5 < p.y && s.location.y+5 > p.y){
                    return s;
                }
            }
        }

        return null;

    }

    public Sector getSectorId(int id){
        return sectors.get(id);
    }

    public Collection<Sector> getSectors(){
        return sectors;
    }

    public void addSector(Sector s){
        sectors.add(s);
    }
}