/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 02:41
 * To change this template use File | Settings | File Templates.
 */
public class Sector {
    protected String id;
    protected Point location;
    protected Player owner;
    protected Collection<MapAsset> assets;

    public Sector(String ID, Point location){
        this.id = ID;
        this.location = location;
        this.assets = new ArrayList<MapAsset>();
    }

    public String getID(){
        return id;
    }
    public Point getLocation(){
        return location;
    }

    public void addAsset(MapAsset a){
        assets.add(a);
    }

    public MapAsset getAssetAt(Point p){

        for(MapAsset ma : assets){
            if(ma.inBounds(p)){
                return ma;
            }
        }

        return null;
    }

    public Collection<MapAsset> getAssets(){
        return assets;
    }

    public void setOwner(Player owner){
        this.owner = owner;
    }

    public Player getOwner(){
        return owner;
    }

    public boolean equals(Object o){
        return false;
    }

    public String toString(){
        return id;
    }
}
