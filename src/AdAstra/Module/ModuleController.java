/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra.Module;

import AdAstra.Sector;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 22:46
 * To change this template use File | Settings | File Templates.
 */
public class ModuleController {
    protected Collection<AbstractModule> modules;

    public ModuleController(){
        this.modules = new ArrayList<AbstractModule>();
    }

    public void add(AbstractModule m){
        modules.add(m);
    }


    public Collection<AbstractModule> getModules(){
        return modules;
    }

    public void buildSector(Sector s){
        for(AbstractModule m : modules){
            m.buildSector(s);
        }
    }
}
