/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra.Module.Planet;

import AdAstra.MapAsset;
import AdAstra.Player;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 23:26
 * To change this template use File | Settings | File Templates.
 */
public final class PlanetAsset extends MapAsset {
    private static final Color[] colours = new Color[]{
            new Color(165, 42, 42),
            new Color(255, 125, 64),
            new Color(0, 199, 140),
            new Color(104, 34, 139)
    };
    private int population;
    private int quality;

    public PlanetAsset(String name, Point location, int quality) {
        super(name, null, location, 10, 15);
        this.population = 0;
        this.quality = quality;
    }

    public Color getColor(){
        return PlanetAsset.colours[quality];
    }
}
