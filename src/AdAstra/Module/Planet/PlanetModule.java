/*
 * Ad Astra - Space based game
 * Copyright (C) 2010 <name of author>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package AdAstra.Module.Planet;

import AdAstra.Module.AbstractModule;
import AdAstra.Sector;

import java.awt.*;
import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: webpigeon
 * Date: 27/12/10
 * Time: 23:25
 * To change this template use File | Settings | File Templates.
 */
public class PlanetModule extends AbstractModule {

    @Override
    public void buildSector(Sector s) {
        Random r = new Random();

        for(int i=0; i<r.nextInt(6); i++){
            s.addAsset( new PlanetAsset("Random Planet", new Point(r.nextInt(440)+10,r.nextInt(440)+10), r.nextInt(4)) );
        }
    }
}
